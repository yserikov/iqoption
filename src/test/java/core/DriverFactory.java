package core;

import com.codeborne.selenide.Configuration;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Before;

import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

import static com.codeborne.selenide.WebDriverRunner.CHROME;

public abstract class DriverFactory {

    private Logger logger = LogManager.getLogger(DriverFactory.class);

    protected Steps sharedSteps = new Steps();

    @Before
    public void setUp() {
        try {
            System.setOut(new PrintStream(System.out, true, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Configuration.reportsFolder = "target/allure-results/reports";
        Configuration.savePageSource = false;
        Configuration.startMaximized = true;
//        Configuration.holdBrowserOpen = true;
        String basedir = System.getProperties().getProperty("basedir");
        if (OSValidator.isWindows()) {
            System.setProperty("webdriver.chrome.driver", basedir + "/src/test/resources/chromedriver_x86.exe");
        } else if (OSValidator.isMac()) {
            System.setProperty("webdriver.chrome.driver", basedir + "/src/test/resources/chromedriver_osx");
        } else if (OSValidator.isUnix()) {
            System.setProperty("webdriver.chrome.driver", basedir + "/src/test/resources/chromedriver_linux");
        }
        Configuration.browser = CHROME;
    }
}