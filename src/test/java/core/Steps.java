package core;

import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import io.qameta.allure.model.Status;
import io.qameta.allure.model.StepResult;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.UUID;

import static com.codeborne.selenide.Screenshots.takeScreenShotAsFile;
import static com.google.common.io.Files.toByteArray;
import static io.qameta.allure.Allure.getLifecycle;

public class Steps {

    private Logger logger = LogManager.getLogger(Steps.class);

    @Step("{0}")
    public void stepInfo(String log) {
        logger.info(log);
    }

    public void stepWarn(String value) {
        final String uuid = UUID.randomUUID().toString();
        final StepResult result = new StepResult().withName(value);
        logger.warn(value);
        getLifecycle().startStep(uuid, result);
        try {
            getLifecycle().updateStep(uuid, s -> s.withStatus(Status.FAILED));
        } finally {
            getLifecycle().stopStep(uuid);
        }
    }

    @Step("Screenshot {0}")
    public void addScreenToReport(String screenName) {
        attachScreenshot();
    }

    @Attachment(value = "Screenshot", type = "image/png")
    public static byte[] attachScreenshot() {
        try {
            return toByteArray(takeScreenShotAsFile());
        } catch (IOException e) {
            return new byte[0];
        }
    }
}