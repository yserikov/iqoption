package pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class HomePage {

    public SelenideElement firstName(){
        return $("[name=first_name]");
    }

    public SelenideElement lastName(){
        return $("[name=last_name]");
    }

    public SelenideElement email(){
        return $("[name=email]");
    }

    public SelenideElement password(){
        return $("[name=password]");
    }

    public SelenideElement acceptBox(){
        return $(".Checkbox__custom");
    }

    public SelenideElement registerErrorBlock(){
        return $("[data-test-id='register-error-block']");
    }

    public SelenideElement loginErrorBlock(){
        return $("[data-test-id='login-error-block']");
    }

    public SelenideElement regButton(){
        return $("[data-test-id='register-submit-button']");
    }

    public SelenideElement loginButton(){
        return $("[data-test-id='login-submit-button']");
    }

    public SelenideElement iqInputError(){
        return $(".iqInput__error");
    }

    public SelenideElement loginSidebar(){
        return $$(".SidebarTab__button").first();
    }

    public SelenideElement regSidebar(){
        return $$(".SidebarTab__button").last();
    }
}