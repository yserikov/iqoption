package pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class IqOptionPage {

    public SelenideElement userEmailProfile(){
        return $(".SidebarProfile__UserEmail");
    }

    public SelenideElement logoutButon(){
        return $("button.SidebarProfile__MenuLink");
    }
}