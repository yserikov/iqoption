package tests;

import core.Steps;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.junit4.DisplayName;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Feature("REST API")
@Story("Предлагается написать несколько типовых тестов на сервис авторизации")
public class RestLoginTest {

    private Logger logger = LogManager.getLogger(RestLoginTest.class);

    protected Steps sharedSteps = new Steps();

    private Response postReq = null;
    private JsonPath jsonRes = null;

    @Test
    @DisplayName("Успешная авторизация")
    @Description("Успешная авторизация")
    public void testOne() {
        postRegisterOne("qa.test.aka@gmail.com", "1q2w3E4R");
        assertNotNull(jsonRes.get("data.ssid"));
        sharedSteps.stepInfo(jsonRes.get("data").toString());
    }

    @Test
    @DisplayName("Передан не верный пароль")
    @Description("Передан не верный пароль")
    public void testTwo() {
        postRegisterOne("qa.test.aka@gmail.com", "123456");
        assertEquals("[Invalid credentials]", jsonRes.get("errors.title").toString());
        assertEquals("[202]", jsonRes.get("errors.code").toString());
        sharedSteps.stepInfo(jsonRes.get("errors").toString());
    }

    @Test
    @DisplayName("Передан не корректный email адрес")
    @Description("Передан не корректный email адрес")
    public void testThree() {
        postRegisterOne("qa.test.aka.gmail.com", "123456");
        assertEquals("[Invalid email]", jsonRes.get("errors.title").toString());
        assertEquals("[1]", jsonRes.get("errors.code").toString());
        sharedSteps.stepInfo(jsonRes.get("errors").toString());
    }

    public void postRegisterOne(String email, String password) {
        postReq =
                given().
                contentType("application/x-www-form-urlencoded; charset=UTF-8").
                header("Cookie", "lang=en_US").
                param("email",email).
                param("password",password).
                when().post("https://auth.iqoption.com/api/v1.0/login").
                then().extract().response();
//        logger.info(postReq.asString());
        jsonRes = new JsonPath(postReq.asString());
    }
}