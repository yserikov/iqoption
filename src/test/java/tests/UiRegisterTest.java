package tests;

import core.DriverFactory;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.junit4.DisplayName;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Test;
import pages.HomePage;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Condition.*;

@Feature("UI Tests")
@Story("Предлагается написать несколько типовых негативных тестов на сервис регистрации")
public class UiRegisterTest extends DriverFactory {

    private Logger logger = LogManager.getLogger(UiRegisterTest.class);

    @Test
    @DisplayName("Передан уже зарегистрированный email")
    @Description("Передан уже зарегистрированный email")
    public void testOne() {
        HomePage homePage = open("https://iqoption.com/en", HomePage.class);
//        sharedSteps.addScreenToReport("Home Page");
        homePage.regSidebar().waitUntil(visible, 15000).click();
        sharedSteps.addScreenToReport("Home Page");
        homePage.firstName().val("iqFname");
        homePage.lastName().val("iqLname");
        homePage.email().val("qa.test.aka@gmail.com");
        homePage.password().val("1q2w3e4r");
        homePage.acceptBox().click();
        homePage.regButton().click();
        homePage.registerErrorBlock().waitUntil(visible, 15000).shouldHave(text("You have already registered"));
        sharedSteps.addScreenToReport("After Form filling");
    }

    @Test
    @DisplayName("Передан не валидный email")
    @Description("Передан не валидный email")
    public void testTwo() {
        HomePage homePage = open("https://iqoption.com/en", HomePage.class);
        homePage.regSidebar().waitUntil(visible, 15000).click();
        sharedSteps.addScreenToReport("Home Page");
        homePage.firstName().val("iqFname");
        homePage.lastName().val("iqLname");
        homePage.email().val("qa.test.aka.gmail.com");
        homePage.password().val("1q2w3e4r");
        homePage.acceptBox().click();
        homePage.regButton().click();
        homePage.iqInputError().waitUntil(visible, 15000).shouldHave(text("Invalid e-mail"));
        sharedSteps.addScreenToReport("After Form filling");
    }

    @Test
    @DisplayName("Передан пароль длиной менее 6-ти символов")
    @Description("Передан пароль длиной менее 6-ти символов")
    public void testThree() {
        HomePage homePage = open("https://iqoption.com/en", HomePage.class);
        homePage.regSidebar().waitUntil(visible, 15000).click();
        sharedSteps.addScreenToReport("Home Page");
        homePage.firstName().val("iqFname");
        homePage.lastName().val("iqLname");
        homePage.email().val("qa.test.aka@gmail.com");
        homePage.password().val("1q2W3");
        homePage.acceptBox().click();
        homePage.regButton().click();
        homePage.iqInputError().waitUntil(visible, 15000).shouldHave(text("Your password must be at least 6 characters long and contain at least 1 letter and 1 digit"));
        sharedSteps.addScreenToReport("After Form filling");
    }

    @Test
    @DisplayName("Не передано имя пользователя")
    @Description("Не передано имя пользователя")
    public void testFour() {
        HomePage homePage = open("https://iqoption.com/en", HomePage.class);
        homePage.regSidebar().waitUntil(visible, 15000).click();
        sharedSteps.addScreenToReport("Home Page");
        homePage.lastName().val("iqLname");
        homePage.email().val("qa.test.aka@gmail.com");
        homePage.password().val("1q2w3e4r");
        homePage.acceptBox().click();
        homePage.regButton().click();
        homePage.iqInputError().waitUntil(visible, 15000).shouldHave(text("Fill in the field"));
        sharedSteps.addScreenToReport("After Form filling");
    }
}