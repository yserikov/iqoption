package tests;

import core.Steps;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.junit4.DisplayName;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

@Feature("REST API")
@Story("Предлагается написать несколько типовых негативных тестов на сервис регистрации")
public class RestRegisterTest {

    private Logger logger = LogManager.getLogger(RestRegisterTest.class);

    protected Steps sharedSteps = new Steps();

    private Response postReq = null;
    private JsonPath jsonRes = null;

    @Test
    @DisplayName("Передан уже зарегистрированный email")
    @Description("Передан уже зарегистрированный email")
    public void testOne() {
        postRegisterOne("iqFname", "iqLname", "qa.test.aka@gmail.com", "123456");
        assertEquals("[You have already registered]", jsonRes.get("message").toString());
        sharedSteps.stepInfo(jsonRes.get("message").toString());
    }

    @Test
    @DisplayName("Передан не валидный email")
    @Description("Передан не валидный email")
    public void testTwo() {
        postRegisterOne("iqFname", "iqLname", "qa.test.aka.gmail.com", "123456");
        assertEquals("[E-mail is not valid]", jsonRes.get("message.email").toString());
        sharedSteps.stepInfo(jsonRes.get("message").toString());
    }

    @Test
    @DisplayName("Передан пароль длиной менее 6-ти символов")
    @Description("Передан пароль длиной менее 6-ти символов")
    public void testThree() {
        postRegisterOne("iqFname", "iqLname", "qa.test1@gmail.com", "12345");
        assertEquals("[Invalid password length]", jsonRes.get("message.password").toString());
        sharedSteps.stepInfo(jsonRes.get("message").toString());
    }

    @Test
    @DisplayName("Не передано имя пользователя")
    @Description("Не передано имя пользователя")
    public void testFour() {
        postRegisterOne("", "iqLname", "qa.test2@gmail.com", "123456");
        assertEquals("[First name is required]", jsonRes.get("message.first_name").toString());
        sharedSteps.stepInfo(jsonRes.get("message").toString());
    }

    public void postRegisterOne(String fName, String lName, String email, String password) {
        postReq =
                given().
                contentType("application/x-www-form-urlencoded; charset=UTF-8").
                header("Cookie", "lang=en_US").
                param("first_name",fName).
                param("last_name",lName).
                param("email",email).
                param("password",password).
                param("tz","Europe/Moscow").
                when().post("https://iqoption.com/api/register").
                then().extract().response();
//        logger.info(postReq.asString());
        jsonRes = new JsonPath(postReq.asString());
    }
}