package tests;

import core.DriverFactory;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.junit4.DisplayName;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Test;
import pages.HomePage;
import pages.IqOptionPage;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.page;

@Feature("UI Tests")
@Story("Предлагается написать несколько типовых тестов на сервис авторизации")
public class UiLoginTest extends DriverFactory {

    private Logger logger = LogManager.getLogger(UiLoginTest.class);

    @Test
    @DisplayName("Успешная авторизация")
    @Description("Успешная авторизация")
    public void testOne() {
        HomePage homePage = open("https://iqoption.com/en", HomePage.class);
        homePage.loginSidebar().waitUntil(visible, 15000).click();
        sharedSteps.addScreenToReport("Home Page before login");
        homePage.email().val("qa.test.aka@gmail.com");
        homePage.password().val("1q2w3E4R");
        homePage.loginButton().click();
        IqOptionPage iqOptionPage = page(IqOptionPage.class);
        iqOptionPage.userEmailProfile().waitUntil(visible, 15000).shouldHave(text("qa.test.aka@gmail.com"));
        sharedSteps.addScreenToReport("IqOption Page after login");
        iqOptionPage.logoutButon().click();
    }

    @Test
    @DisplayName("Передан не верный пароль")
    @Description("Передан не верный пароль")
    public void testTwo() {
        HomePage homePage = open("https://iqoption.com/en", HomePage.class);
        homePage.loginSidebar().waitUntil(visible, 15000).click();
        sharedSteps.addScreenToReport("Home Page before login");
        homePage.email().val("qa.test.aka@gmail.com");
        homePage.password().val("1q2w3e4r");
        homePage.loginButton().click();
        homePage.loginErrorBlock().waitUntil(visible, 15000).shouldHave(text("Invalid login or password"));
        sharedSteps.addScreenToReport("After Form filling");
    }

    @Test
    @DisplayName("Передан не корректный email адрес")
    @Description("Передан не корректный email адрес")
    public void testThree() {
        HomePage homePage = open("https://iqoption.com/en", HomePage.class);
        homePage.loginSidebar().waitUntil(visible, 15000).click();
        sharedSteps.addScreenToReport("Home Page before login");
        homePage.email().val("qa.test.aka.gmail.com");
        homePage.password().val("1q2w3E4R");
        homePage.loginButton().click();
        homePage.iqInputError().waitUntil(visible, 15000).shouldHave(text("Invalid e-mail"));
        sharedSteps.addScreenToReport("After Form filling");
    }
}